package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

type monitor struct {
	Name string
	Make string
	Model string
	Serial string
}

type pair struct {
	Name string
	Bg string
}

func main() {
	argStr := ""
	for _, a := range os.Args[1:] {
		argStr += a + " "
	}
	argStr = strings.TrimSpace(argStr)

	argStr = strings.ReplaceAll(argStr, "'", "")

	pairsTmp := strings.Split(argStr, ",")

	pairs := make([]pair, 0)
	for _, p := range pairsTmp {
		parts := strings.Split(p, ":")
		pairs = append(pairs, pair{
			Name: parts[0],
			Bg:   parts[1],
		})
	}
	
	b, e := exec.Command("swaymsg", "-t", "get_outputs").CombinedOutput()
	if e != nil {
		panic(e)
	}
	
	var mons []monitor
	e = json.Unmarshal(b, &mons)
	if e != nil {
		panic(e)
	}

	args := []string{"-f"}
	
	for _, p := range pairs {
		for _, m := range mons {
			if fmt.Sprintf("%s %s %s", m.Make, m.Model, m.Serial) == p.Name {
				args = append(args, fmt.Sprintf("--image=%s:%s", m.Name, p.Bg))
				break
			}
		}
	}

	b, e = exec.Command("swaylock", args...).CombinedOutput()
	if e != nil {
		fmt.Println(string(b))
		panic(e)
	}
	
}
